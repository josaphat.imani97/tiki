<?php

namespace Tiki\Exceptions\BigBlueButton;

use Exception;

class ServerSaltKeyException extends Exception
{
}
